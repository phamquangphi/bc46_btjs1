/**
 * bài 1
 * input: số ngày bạn làm
 * output: tổng lương của bạn = ngày làm + 100.00
 */

function tinh(){
    var soNgaylam = document.getElementById("soNgay").value*1 ;
     console.log(soNgaylam)
    var tongLuong = soNgaylam * 100.000;
    document.getElementById("luongTong").innerHTML = `${tongLuong} VND`;
}
/**
 * Bài 2
 * input: số a, b, c, d, e;
 * output: tổng Trung Bình = (a + b + c + d + e)/5
 */
function tong(){
    var a = document.getElementById("soThuNhat").value*1;
    console.log(a);
    var b = document.getElementById("soThuHai").value*1;
    console.log(b);
    var c = document.getElementById("soThuBa").value*1;
    console.log(c);
    var d = document.getElementById("soThuTu").value*1;
    console.log(d);
    var e = document.getElementById("soThuNam").value*1;
    console.log(e);
    var tongTB = (a + b + c + d + e)/5;
    document.getElementById("tongTrungbinh").innerHTML=tongTB;


}


/**
 * Bài 3
 * input: nhập dữ liệu USD cần đổi
 * output: Quy đổi tiền VND = sotien * 23500
 */
function tien(){
    var soTien = document.getElementById("soTien").value*1;
    console.log(soTien); 
    var quyDoi = soTien * 23.500;
    document.getElementById("soTienvnd").innerHTML=`${quyDoi} VND`;
}

/**
 * Bài 4
 * input: nhập chiều dài CD và chiều rộng CR
 * output: CVHCN = (CD + CR)*2
 *         DTHCN = CD * CR
 */
function HCN(){
    
    var cD = document.getElementById("chieuDai").value*1;
    console.log('chiều dài', cD);
   var cR= document.getElementById("chieuRong").value*1;
    console.log('chiều rộng', cR);
    
    var cV = (cD + cR) * 2;
    document.getElementById("CV").innerText = cV;
    var DT = cD * cR;
    document.getElementById("DT").innerText= DT;
}


/**
 * Bài 5
 * input: nhập con số bất kì
 * output: tổng của số
 */
function so(){
    var conSo=document.getElementById("conSo").value*1;
     console.log(conSo)

    var hangDonvi = conSo % 10;
    var hangChuc = conSo/10;

    var Tong = Math.floor(hangDonvi + hangChuc);
    document.getElementById("TongConso").innerHTML= Tong;
}